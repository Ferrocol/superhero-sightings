# Superhero Sightings

A full-stack CRUD web application that tracks the latest superhero sightings.
Stores data about superheros, their superpowers, organizations, and sighting information.
Utilizes MySQL, Java, Apache Tomcat, Spring, Javascript, jQuery, Ajax, HTML, CSS, Bootstrap, and Materialize.

## Getting Started

### Prerequisites
Apache Tomcat 8.5.3
MySQL

## Built With
* [Netbeans](https://netbeans.org/) - A free-open source IDE
* [Maven](https://maven.apache.org/) - Dependency Management
* [MySQL](https://www.mysql.com/) - Open-source relational database management
* [Bootstrap](https://getbootstrap.com/docs/3.3/) - Responsive HTML, CSS, JS Framework
* [Materialize](http://materializecss.com/) - A modern responsive front-end framework based on Material Design
