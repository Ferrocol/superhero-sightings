$(document).ready(function() {
    loadLocations();
    $('#show-more-locations-btn').click(function() {
        showNextTenLocations();
    });
    $('input[type="radio"][name="enter-by"]').change(function() {
        switch ($(this).val()) {
            case 'address':
                $('div#enter-by-geolocation').hide();
                $('div#general').show();
                $('div#enter-by-address').show();
                $('div#submit-button').show();
                break;
            case 'geolocation':
                $('div#enter-by-address').hide();
                $('div#general').show();
                $('div#enter-by-geolocation').show();
                $('div#submit-button').show();
                break;
        }
    });
    $('#new-location-form').on('reset', function() {
        $('#general').hide();
        $('#enter-by-address').hide();
        $('#enter-by-geolocation').hide();
        $('#submit-button').hide();
    });
    $('#new-location-form').submit(function() {
        var enterBy = $('input[type="radio"][name="enter-by"]:checked');
        var isValid = false;
        if (!$('#name').val().trim()) {
            alert('Enter a location name.');
        } else if (enterBy.val() === 'address') {
            if (!$('#streetAddress').val().trim()) {
                alert('Enter a street address.');
            } else if (!$('#city').val().trim()) {
                alert('Enter a city.');
            } else if (!$('#state').val().trim()) {
                alert('Enter a state or province.');
            } else if (!$('#country').val().trim()) {
                alert('Enter a country.');
            } else {
                isValid = true;
            }
        } else if (enterBy.val() === 'geolocation') {
            if (!$('#latitude').val().trim()) {
                alert('Enter a latitude value in degrees.');
            } else if (!$('#longitude').val().trim()) {
                alert('Enter a longitude value in degrees.');
            } else {
                isValid = true;
            }
        }
        if (isValid) {
            addNewLocation();
        }
        return false;
    });
    $('#location-list').on('click', 'div.edit', function() {
        fillEditForm($(this).attr('id').substring(4));
    });
    $('#submit-edit-location').click(function() {
        var isValid = false;
        var name = $('#name').val().trim();
        var streetAddress = $('#streetAddress').val().trim();
        var city = $('#city').val().trim();
        var state = $('#state').val().trim();
        var country = $('#country').val().trim();
        var latitude = $('#latitude').val().trim();
        var longitude = $('#longitude').val().trim();
        if (!name) {
            alert('Enter a location name.');
        } else if (!streetAddress && !city && !state && !country && !latitude && !longitude) {
            alert('Location information cannot be empty.');
        } else {
            isValid = true;
        }
        if (isValid) {
            editLocation();
        }
        return false;
    });
    $('#cancel-edit-location').click(function() {
        clearEditForm();
    });
    $('#location-list').on('click', 'div.delete', function() {
        if (window.confirm('Are you sure you want to delete this organization?')) {
            deleteLocation($(this).attr('id').substring(6));
        } else {
            alert('Delete cancelled.');
        }
    });
});

var numberOfLocations = 0;
var visibleLocations = 0;
function loadLocations() {
    $('#location-list').empty();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/locations',
        success: function(data, status) {
            $.each(data, function(index, location) {
                numberOfLocations++;
                var id = location.id;
                $('#location-list').append(
                        '<div class="location" id="' + id + '">'
                        + '<div class="edit" id="edit' + id + '">'
                        + '<a href="#"><i class="material-icons">edit</i></a></div>'
                        + '<div class="delete" id="delete' + id + '">'
                        + '<a href="#"><i class="material-icons">delete_forever</i></a></div>'
                        + '</div>');
                var locationString = '<h5>' + location.name + '</h5>';
                if (location.description != null) {
                    locationString += '<h6>' + location.description + '</h6>';
                }
                locationString += '<p>';
                if (location.streetAddress != null) {
                    locationString += location.streetAddress + ', ';
                }
                if (location.city != null) {
                    locationString += location.city + ', ';
                }
                if (location.state != null) {
                    locationString += location.state + ', ';
                }
                if (location.country != null) {
                    locationString += location.country + ' ';
                }
                locationString += '</p>';
                if (location.latitudeDeg != null && location.longitudeDeg != null) {
                    locationString += '<i class="material-icons">location_on</i>&nbsp;'
                            + location.latitudeDeg + ',' + location.longitudeDeg;
                }
                $('#location-list div#' + id).append('<p id="location'
                        + id + '" value="' + id + '">' + locationString + '</p>');
                $('#location-list div#' + id).addClass('hidden');
            });
            showNextTenLocations();
        },
        error: function() {
            $('#load-error').show();
        }
    });
}

function showNextTenLocations() {
    if (Number(numberOfLocations - visibleLocations) > 10) {
        var count = 0;
        $.each($('#location-list div.location'), function() {
            if (count < 10) {
                if ($(this).hasClass('hidden')) {
                    $(this).removeClass('hidden');
                    count++;
                    visibleLocations++;
                }
            }
        });
        if (Number(numberOfLocations - visibleLocations) > 0) {
            $('#show-more-locations-btn').show();
        } else {
            $('#show-more-locations-btn').hide();
        }
    } else {
        $.each($('#location-list div.location'), function() {
            if ($(this).hasClass('hidden')) {
                $(this).removeClass('hidden');
                visibleLocations++;
            }
        });
        $('#show-more-locations-btn').hide();
    }
}

function addNewLocation() {
    var latitudeDeg, longitudeDeg;
    if (!$('#latitude').val()) {
        latitudeDeg = null;
    } else {
        latitudeDeg = Number($('#latitude').val());
    }
    if (!$('#longitude').val()) {
        longitudeDeg = null;
    } else {
        longitudeDeg = Number($('#longitude').val());
    }

    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/SuperheroSightings/location',
        data: JSON.stringify({
            name: $('#name').val(),
            description: $('#description').val(),
            streetAddress: $('#streetAddress').val(),
            city: $('#city').val(),
            state: $('#state').val(),
            country: $('#country').val(),
            latitudeDeg: latitudeDeg,
            longitudeDeg: longitudeDeg
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function() {
            $('#add-location-error').hide();
            $('#add-location-success').show();
            $('#new-location-form')[0].reset();
            loadLocations();
        },
        error: function() {
            $('#add-location-success').hide();
            $('#add-location-error').show();
        }
    });
}

function fillEditForm(id) {
    $('div.form-group h5').text('Edit Location');
    $('#enter-by-div').hide();
    $('#general').show();
    $('#enter-by-address').show();
    $('#enter-by-geolocation').show();
    $('#submit-button').hide();
    $('.edit-buttons').show();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/location/' + id,
        success: function(location, status) {
            $('#location-id').val(id);
            $('#name').val(location.name);
            $('#description').val(location.description);
            $('#streetAddress').val(location.streetAddress);
            $('#city').val(location.city);
            $('#state').val(location.state);
            $('#country').val(location.country);
            $('#latitude').val(location.latitudeDeg);
            $('#longitude').val(location.longitudeDeg);
        },
        error: function() {
            alert('ERROR: Unable to populate the edit form.');
        }
    });
}

function editLocation() {
    var latitudeDeg, longitudeDeg;
    if (!$('#latitude').val()) {
        latitudeDeg = null;
    } else {
        latitudeDeg = Number($('#latitude').val());
    }
    if (!$('#longitude').val()) {
        longitudeDeg = null;
    } else {
        longitudeDeg = Number($('#longitude').val());
    }
    $.ajax({
        type: 'PATCH',
        url: 'http://localhost:8080/SuperheroSightings/location/' + $('#location-id').val(),
        data: JSON.stringify({
            id: $('#location-id').val(),
            name: $('#name').val(),
            description: $('#description').val(),
            streetAddress: $('#streetAddress').val(),
            city: $('#city').val(),
            state: $('#state').val(),
            country: $('#country').val(),
            latitudeDeg: latitudeDeg,
            longitudeDeg: longitudeDeg
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function() {
            $('#edit-location-error').hide();
            $('#edit-location-success').show();
            loadLocations();
            clearEditForm();
        },
        error: function() {
            $('#edit-location-success').hide();
            $('#edit-location-error').show();
        }
    });
}

function clearEditForm() {
    $('div.form-group form')[0].reset();
    $('div.form-group h5').text('Add New Location');
    $('#enter-by-div').show();
    $('#general').hide();
    $('#enter-by-address').hide();
    $('#enter-by-geolocation').hide();
    $('#submit-button').hide();
    $('.edit-buttons').hide();
}

function deleteLocation(id) {
    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/SuperheroSightings/location/' + id,
        success: function() {
            loadLocations();
        },
        error: function() {
            alert('ERROR: Unable to delete location.');
        }
    });
}