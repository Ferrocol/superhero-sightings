$(document).ready(function() {
    loadLastTenSightings();
});

function loadLastTenSightings() {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/sightings/recent',
        success: function(data, status) {
            $.each(data, function(index, sighting) {
                $('#news-feed-div').append('<div class="sighting' + index + '">'
                        + '<h6>' + new Date(sighting.dateTime).toDateString() + '</h6>'
                        + '</div><hr/>');
                $('#news-feed-div .sighting' + index).append('<p>'
                        + sighting.location.name + ' (' + sighting.location.city + ', '
                        + sighting.location.state + ', ' + sighting.location.country
                        + ')</p>');
                for (i = 0; i < sighting.supers.length; i++) {
                    $('#news-feed-div .sighting' + index).append('<p>'
                            + '<i class="material-icons">person_pin_circle</i>&nbsp;'
                            + sighting.supers[i].name + '</p>');
                }
            });
        },
        error: function() {
            $('#load-error').show();
        }
    });
}