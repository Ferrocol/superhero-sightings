$(document).ready(function() {
    loadSupers();
    loadPowers();
    loadOrganizations();
    $('#add-power-button').click(function() {
        var selection = $('#power-select').val();
        if (powerIdMap[selection] != null) {
            $('#power-list').append('<li id="' + selection + '" value="'
                    + powerIdMap[selection] + '">' + selection
                    + '<a href="#"><i id="remove-power-button" class="material-icons">'
                    + 'cancel</i></a></li>');
            delete powerIdMap[selection];
            delete powers[selection];
            $('#power-select').val('');
        }
    });
    $('#power-list').on('click', '#remove-power-button', function() {
        var element = $(this).parent().parent();
        var power = element.attr('id');
        var id = element.val();
        powers[power] = null;
        powerIdMap[power] = id;
        element.remove();
    });
    $('#new-super-form').on('submit', function(event) {
        alert('Form submit');
        var isValid = false;
        if (!$('#name').val().trim()) {
            alert('Enter a name.');
        } else if (!$('#description').val().trim()) {
            alert('Enter a description.');
        } else {
            isValid = true;
        }
        if (isValid) {
            addNewSuper();
        }
        return false;
    });
    $('#super-list').on('click', 'div.default', function() {
        var id = $(this).attr('id');
        $('#super-list div#' + id + 'exp').slideToggle('slow');
        var expandArrow = $(this).children('div.arrow').children('i');
        if (expandArrow.text() === 'expand_more') {
            expandArrow.text('expand_less');
        } else if (expandArrow.text() === 'expand_less') {
            expandArrow.text('expand_more');
        }
    });
    $('#super-list').on('click', 'div.edit', function() {
        loadEditForm($(this).attr('id').substring(4));
    });
    $('#edit-add-power-button').click(function() {
        var selection = $('#edit-power-select').val();
        if (powerIdMap[selection] != null) {
            $('#edit-power-list').append('<li id="' + selection + '" value="'
                    + powerIdMap[selection] + '">' + selection
                    + '<a href="#"><i id="edit-remove-power-button" class="material-icons">'
                    + 'cancel</i></a></li>');
            delete powerIdMap[selection];
            delete powers[selection];
            $('#edit-power-select').val('');
        }
    });
    $('#edit-power-list').on('click', '#edit-remove-power-button', function() {
        var element = $(this).parent().parent();
        var power = element.attr('id');
        var id = element.val();
        powers[power] = null;
        powerIdMap[power] = id;
        element.remove();
    });
    $('#edit-super-form').on('submit', function() {
        var isValid = false
        if (!$('#edit-name').val().trim()) {
            alert('Enter a name.');
        } else if (!$('#edit-description').val().trim()) {
            alert('Enter a description.');
        } else {
            isValid = true;
        }
        if (isValid) {
            editSuper();
        }
        return false;
    });

    $('#add-new-power-btn').click(function() {
        if (!$('#power-description').val().trim()) {
            alert('Super power description cannot be empty.');
        } else {
            addNewPower();
        }
    });
    $('#all-powers-list').on('click', 'a.edit-master-power', function() {
        $(this).parent().parent('div.power').hide();
        var id = $(this).parent().parent('div.power').attr('id');
        $('#all-powers-list div.edit-power#' + id).show();
        $('#all-powers-list input#edit-power-' + id).val($(this).attr('id'));
    });
    $('#all-powers-list').on('click', 'a.delete-master-power', function() {
        if (window.confirm('Are you sure you want to delete this super power?')) {
            deletePower($(this).parent().parent('div.power').attr('id'));
        } else {
            alert('Delete cancelled.');
        }
    });
    $('#all-powers-list').on('click', 'a.save-edit-power', function() {
        if (!$(this).parent('div.edit-power').children('input').val().trim()) {
            alert('Super power description cannot be empty.');
        } else {
            editPower($(this).parent('div.edit-power').attr('id'));
        }
    });
    $('#all-powers-list').on('click', 'a.cancel-edit-power', function() {
        var id = $(this).parent('div.edit-power').attr('id');
        $(this).parent('div.edit-power').hide();
        $('#all-powers-list div.power#' + id).show();
    });
    $('#show-more-powers-btn').click(function() {
        showNextTenPowers();
    });

    $('#super-list').on('click', 'div.delete', function() {
        if (window.confirm('Are you sure you want to delete this super?')) {
            deleteSuper($(this).attr('id').substring(6));
        } else {
            alert('Delete cancelled.');
        }
    });
});

function loadSupers() {
    $('#super-list').empty();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/supers',
        success: function(data, status) {
            $.each(data, function(index, superPerson) {
                var id = superPerson.id;
                $('#super-list').append('<div class="edit" id="edit' + id + '">'
                        + '<a href="#" data-toggle="modal" data-target="#edit-super-modal" id="edit-super-modal-btn">'
                        + '<i class="material-icons">edit</i></a></div>'
                        + '<div class="delete" id="delete' + id + '">'
                        + '<a href="#" id="delete-master-super">'
                        + '<i class="material-icons">delete_forever</i></a></div>'
                        + '<div class="default" id="' + id
                        + '"></div>');
                if (id < 8) { // TODO hard-coded for now because only 7 pictures have been saved
                    $('#super-list div#' + id).append('<h6><img src="images/superId' + id
                            + '.jpg" alt="" class="circle">&nbsp;' + superPerson.name
                            + ' (' + superPerson.description + ')</h6>');
                } else {
                    $('#super-list div#' + id).append('<h6><img class="circle"'
                            + 'src="http://via.placeholder.com/250x250">&nbsp;'
                            + superPerson.name + ' (' + superPerson.description
                            + ')</h6>');
                }
                var powers = superPerson.powers;
                var orgs = superPerson.organizations;
                if (powers.length !== 0 || orgs.length !== 0) {
                    $('#super-list div#' + id).append('<div class="arrow">'
                            + '<i id="arrow" class="material-icons">expand_more</i></div>');
                    $('#super-list div#' + id).append('<div class="expand" id="'
                            + id + 'exp"></div>');
                    if (powers.length !== 0) {
                        $.each(powers, function(i, power) {
                            $('#super-list div#' + id + 'exp').append('<p>'
                                    + '<span class="glyphicon glyphicon-flash"></span>'
                                    + '&nbsp;' + power.description + '</p>');
                        });
                    }
                    if (orgs.length !== 0) {
                        $.each(orgs, function(i, org) {
                            $('#super-list div#' + id + 'exp').append('<p>'
                                    + '<i class="material-icons">group</i>&nbsp;'
                                    + org.name + '</p>');
                        });
                    }
                }
            });
        },
        error: function() {
            $('#load-error').show();
        }
    });
}

var powers = {};
var powerIdMap = {};
var numberOfPowers = 0;
var visiblePowers = 0;

function loadPowers() {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/powers',
        success: function(data, status) {
            $('#all-powers-list').empty();
            $.each(data, function(index, power) {
                numberOfPowers++;
                var id = power.id;
                var desc = power.description
                powers[desc] = null;
                powerIdMap[desc] = id;
                $('#all-powers-list').append('<div class="power" id="' + id
                        + '"><p>' + desc
                        + '<a href="#" class="edit-master-power" id="' + desc + '">'
                        + '<i class="material-icons">edit</i></a>'
                        + '<a href="#" class="delete-master-power">'
                        + '<i class="material-icons">delete_forever</i></a></p></div>'
                        + '<div class="edit-power" id="' + id + '">'
                        + '<a href="#" class="save-edit-power"><i class="material-icons">save</i></a>'
                        + '<a href="#" class="cancel-edit-power"><i class="material-icons">cancel</i></a>'
                        + '<input type="text" id="edit-power-' + id + '" required>'
                        + '</div>');
                $('#all-powers-list div.power#' + id).addClass('hidden');
            });
            showNextTenPowers();
            $('#power-select').autocomplete({
                data: powers,
                limit: 10,
                onAutocomplete: function(val) {
                },
                minLength: 1
            });
        },
        error: function() {
            $('#powers-load-error').show();
        }
    });
}

function loadOrganizations() {
    $('#org-select').empty().html(
            '<option value="" disabled>Select Organization(s)</option>');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/organizations',
        success: function(data, status) {
            $.each(data, function(index, org) {
                $('#org-select').append('<option value="' + org.id + '">'
                        + org.name + '</option>');
            });
            $('#org-select').material_select();
        },
        error: function() {
            $('#organizations-load-error').show();
        }
    });
}

function addNewSuper() {
    alert('Hooray');
    var jsonPowers = [];
    $('#power-list').children().each(function() {
        jsonPowers.push({id: Number($(this).val())});
    });
    var jsonOrganizations = [];
    $('#org-select option:selected').each(function() {
        jsonOrganizations.push({id: Number($(this).val())});
    });

    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/SuperheroSightings/super',
        data: JSON.stringify({
            name: $('#name').val(),
            description: $('#description').val(),
            powers: jsonPowers,
            organizations: jsonOrganizations
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function() {
            $('#add-super-error').hide();
            $('#add-super-success').show();
            loadSupers();
            loadPowers();
        },
        error: function() {
            $('#add-super-success').hide();
            $('#add-super-error').show();
        }
    });
}

function loadEditForm(id) {
    loadPowers();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/super/' + id,
        success: function(superPerson, status) {
            $('#super-id').val(superPerson.id);
            $('#edit-name').val(superPerson.name);
            $('#edit-description').val(superPerson.description);
            $('#edit-power-list').empty();
            $.each(superPerson.powers, function(index, power) {
                var description = power.description;
                $('#edit-power-list').append('<li id="' + description + '" value="'
                        + powerIdMap[description] + '">' + description
                        + '<a href="#"><i id="edit-remove-power-button" class="material-icons">'
                        + 'cancel</i></a></li>');
                delete powerIdMap[description];
                delete powers[description];
            });
            $('#edit-power-select').autocomplete({
                data: powers,
                limit: 10,
                onAutocomplete: function(val) {
                },
                minLength: 1
            });

            $('#edit-org-select').empty().html('<option value="" disabled>Select Organization(s)</option>');
            var superOrgs = [];
            $.each(superPerson.organizations, function(index, org) {
                superOrgs.push(org.id);
            });
            $.ajax({
                type: 'GET',
                url: 'http://localhost:8080/SuperheroSightings/organizations',
                success: function(data, status) {
                    $.each(data, function(index, org) {
                        if (jQuery.inArray(org.id, superOrgs) !== -1) {
                            $('#edit-org-select').append('<option value="'
                                    + org.id + '" selected>' + org.name + '</option>');
                        } else {
                            $('#edit-org-select').append('<option value="'
                                    + org.id + '">' + org.name + '</option>');
                        }

                    });
                    $('#edit-org-select').material_select();
                },
                error: function() {
                    $('#edit-organizations-load-error').show();
                }
            });
        },
        error: function() {
            alert('ERROR: Unable to load super person data.');
        }
    });
}

function editSuper() {
    var jsonPowers = [];
    $('#edit-power-list').children().each(function() {
        jsonPowers.push({id: Number($(this).val())});
    });
    var jsonOrganizations = [];
    $('#edit-org-select option:selected').each(function() {
        jsonOrganizations.push({id: Number($(this).val())});
    });

    $.ajax({
        type: 'PATCH',
        url: 'http://localhost:8080/SuperheroSightings/super/' + $('#super-id').val(),
        data: JSON.stringify({
            id: $('#super-id').val(),
            name: $('#edit-name').val(),
            description: $('#edit-description').val(),
            powers: jsonPowers,
            organizations: jsonOrganizations
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function() {
            $('#edit-super-error').hide();
            $('#edit-super-success').show();
            loadSupers();
            loadPowers();
        },
        error: function() {
            $('#edit-super-success').hide();
            $('#edit-super-error').show();
        }
    });
}

function deleteSuper(id) {
    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/SuperheroSightings/super/' + id,
        success: function() {
            loadSupers();
        },
        error: function() {
            alert('ERROR: Unable to delete super person.');
        }
    });
}

function addNewPower() {
    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/SuperheroSightings/power',
        data: JSON.stringify({
            description: $('#power-description').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function() {
            $('#add-power-error').hide();
            $('#add-power-success').show();
            loadPowers();
        },
        error: function() {
            $('#add-power-success').hide();
            $('#add-power-error').show();
        }
    });
}

function editPower(id) {
    $.ajax({
        type: 'PATCH',
        url: 'http://localhost:8080/SuperheroSightings/power/' + id,
        data: JSON.stringify({
            id: id,
            description: $('#all-powers-list input#edit-power-' + id).val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function() {
            loadPowers();
        },
        error: function() {
            alert('ERROR: Unable to update super power.');
        }
    });
}

function deletePower(id) {
    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/SuperheroSightings/power/' + id,
        success: function() {
            loadPowers();
        },
        error: function() {
            alert('ERROR: Unable to delete super power.');
        }
    });
}

function showNextTenPowers() {
    if (Number(numberOfPowers - visiblePowers) > 10) {
        var count = 0;
        $.each($('#all-powers-list div.power'), function() {
            if (count < 10) {
                if ($(this).hasClass('hidden')) {
                    $(this).removeClass('hidden');
                    count++;
                    visiblePowers++;
                }
            }
        });
        if (Number(numberOfPowers - visiblePowers) > 0) {
            $('#show-more-powers-btn').show();
        } else {
            $('#show-more-powers-btn').hide();
        }
    } else {
        $.each($('#all-powers-list div.power'), function() {
            if ($(this).hasClass('hidden')) {
                $(this).removeClass('hidden');
                visiblePowers++;
            }
        });
        $('#show-more-powers-btn').hide();
    }
}