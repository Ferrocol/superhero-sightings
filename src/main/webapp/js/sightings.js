$(document).ready(function() {
    loadLocations();
    loadSupers();
    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: 15,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false
    });
    $('.timepicker').pickatime({
        default: 'now',
        fromnow: 0,
        twelvehour: false,
        donetext: 'OK',
        cleartext: 'Clear',
        canceltext: 'Cancel',
        autoclose: false,
        ampmclickable: true,
        aftershow: function() {}
    });
    $('#show-more-locations-btn').click(function() {
        showNextTenLocations();
    });
    $('#search-by-super-button').click(function() {
        loadSightingsBySuper();
    });
    $('#search-by-date-button').click(function() {
        loadSightingsByDate();
    });
    $('#new-sighting-form').on('submit', function() {
        var isValid = false;
        var supers = [];
        $('#super-select option:selected').each(function() {
            supers.push($(this).val());
        });
        if (!$('.datepicker').val()) {
            alert('Select a date.');
        } else if (!$('.timepicker').val()) {
            alert('Select a time.');
        } else if (supers.length == 0) {
            alert('Select at least one super person.');
        } else if (!$('#all-locations-list input[type="radio"][name="location-select"]:checked').val()) {
            alert('Select a location.');
        } else {
            isValid = true;
        }
        if (isValid) {
            addNewSighting();
        }
        return false;
    });
    $('#search-results-div').on('click', 'div.edit', function() {
        fillEditForm($(this).attr('id').substring(4));
    });
    $('#submit-edit-sighting').click(function() {
        var isValid = false;
        var supers = [];
        $('#super-select option:selected').each(function() {
            supers.push($(this).val());
        });
        if (!$('.datepicker').val()) {
            alert('Select a date.');
        } else if (!$('.timepicker').val()) {
            alert('Select a time.');
        } else if (supers.length == 0) {
            alert('Select at least one super person.');
        } else if (!$('#all-locations-list input[type="radio"][name="location-select"]:checked').val()) {
            alert('Select a location.');
        } else {
            isValid = true;
        }
        if (isValid) {
            editSighting();
        }
    });
    $('#cancel-edit-sighting').click(function() {
        clearEditForm();
    });
    $('#search-results-div').on('click', 'div.delete', function() {
        if (window.confirm('Are you sure you want to delete this sighting?')) {
            deleteSighting($(this).attr('id').substring(6));
        } else {
            alert('Delete cancelled.');
        }
    });
});

function loadSupers() {
    $('#super-select').empty().html(
            '<option value="" disabled>Select Super(s)</option>');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/supers',
        success: function(data, status) {
            var supers = {};
            $.each(data, function(index, superPerson) {
                var id = superPerson.id;
                var name = superPerson.name;
                $('#super-select').append('<option value="' + id + '">'
                        + name + ' (' + superPerson.description
                        + ')</option>');
                supers[name + ' (' + superPerson.description + ')'] =
                        'images/superId' + id + '.jpg';
                superIdMap[name + ' (' + superPerson.description + ')'] = id;
            });
            $('#super-select').material_select();
            $('#search-by-super').autocomplete({
                data: supers,
                limit: 10,
                onAutocomplete: function(val) {
                },
                minLength: 1
            });
        },
        error: function() {
            $('#supers-load-error').show();
            $('#super-select').hide();
        }
    });
}
var numberOfLocations = 0;
var visibleLocations = 0;
function loadLocations() {
    $('#all-locations-list').empty();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/locations',
        success: function(data, status) {
            $.each(data, function(index, location) {
                numberOfLocations++;
                var id = location.id;
                $('#all-locations-list').append(
                        '<p class="location" id="' + id + '"></p>');
                var locationString = location.name;
                if (location.description != null) {
                    locationString += ' (' + location.description + ')';
                }
                if (location.streetAddress != null) {
                    locationString += ', ' + location.streetAddress;
                }
                if (location.city != null) {
                    locationString += ', ' + location.city;
                }
                if (location.state != null) {
                    locationString += ', ' + location.state;
                }
                locationString += ', ' + location.country;
                if (location.latitudeDeg != null && location.longitudeDeg != null) {
                    locationString += '; geo: '
                            + location.latitudeDeg + ',' + location.longitudeDeg;
                }
                $('#all-locations-list p#' + id).append(
                        '<input class="with-gap" name="location-select" type="radio" id="location'
                        + id + '" value="' + id + '" /><label for="location'
                        + id + '">' + locationString + '</label>');
                $('#all-locations-list p#' + id).addClass('hidden');
            });
            showNextTenLocations();
        },
        error: function() {
            $('#all-locations-load-error').show();
        }
    });
}

function showNextTenLocations() {
    if (Number(numberOfLocations - visibleLocations) > 10) {
        var count = 0;
        $.each($('#all-locations-list p.location'), function() {
            if (count < 10) {
                if ($(this).hasClass('hidden')) {
                    $(this).removeClass('hidden');
                    count++;
                    visibleLocations++;
                }
            }
        });
        if (Number(numberOfLocations - visibleLocations) > 0) {
            $('#show-more-locations-btn').show();
        } else {
            $('#show-more-locations-btn').hide();
        }
    } else {
        $.each($('#all-locations-list div.location'), function() {
            if ($(this).hasClass('hidden')) {
                $(this).removeClass('hidden');
                visibleLocations++;
            }
        });
        $('#show-more-locations-btn').hide();
    }
}

var superIdMap = {};

function addNewSighting() {
    var dateTime = Date.parse($('.datepicker').val() + ' ' + $('.timepicker').val());
    var locationId = Number($('#all-locations-list input[type="radio"][name="location-select"]:checked').val());
    var jsonSupers = [];
    $('#super-select option:selected').each(function() {
        jsonSupers.push({id: Number($(this).val())});
    });
    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/SuperheroSightings/sighting',
        data: JSON.stringify({
            dateTime: dateTime,
            locationId: locationId,
            supers: jsonSupers
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function() {
            $('#add-sighting-error').hide();
            $('#add-sighting-success').show();
            $('#new-sighting-form')[0].reset();
        },
        error: function() {
            $('#add-sighting-success').hide();
            $('#add-sighting-error').show();
        }
    });
}

function loadSightingsBySuper() {
    $('#search-results-div').empty();
    var superId = superIdMap[$('#search-by-super').val()];
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/sightings/super/' + superId,
        success: function(data, status) {
            $('#load-error').hide();
            $.each(data, function(index, sighting) {
                $('#search-results-div').append('<div class="' + sighting.id + '">'
                        + '<h6>' + new Date(sighting.dateTime).toDateString() + '</h6>'
                        + '<div class="edit" id="edit' + sighting.id + '">'
                        + '<a href="#"><i class="material-icons">edit</i></a></div>'
                        + '<div class="delete" id="delete' + sighting.id + '">'
                        + '<a href="#"><i class="material-icons">delete_forever</i></a></div>'
                        + '</div><hr/>');
                var locationString = '<p>' + sighting.location.name + ' ';
                if (sighting.location.description != null) {
                    locationString += '(' + sighting.location.description + ') ';
                }
                if (sighting.location.streetAddress != null) {
                    locationString += sighting.location.streetAddress + ', ';
                }
                if (sighting.location.city != null) {
                    locationString += sighting.location.city + ', ';
                }
                if (sighting.location.state != null) {
                    locationString += sighting.location.state + ', ';
                }
                if (sighting.location.country != null) {
                    locationString += sighting.location.country + ' ';
                }
                if (sighting.location.latitudeDeg != null && sighting.location.longitudeDeg != null) {
                    locationString += 'geo: ' + sighting.location.latitudeDeg + ',' + sighting.location.longitudeDeg;
                }
                locationString += '</p>';
                $('#search-results-div .' + sighting.id).append(locationString);
                ;
                for (i = 0; i < sighting.supers.length; i++) {
                    $('#search-results-div .' + sighting.id).append('<p>'
                            + '<i class="material-icons">person_pin_circle</i>&nbsp;'
                            + sighting.supers[i].name + '</p>');
                }
            });
        },
        error: function() {
            $('#load-error').show();
        }
    });
}

function loadSightingsByDate() {
    $('#search-results-div').empty();
    var date = Date.parse($('#search-by-date').val());
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/sightings/date/' + date,
        success: function(data, status) {
            $('#load-error').hide();
            $.each(data, function(index, sighting) {
                $('#search-results-div').append('<div class="' + sighting.id + '">'
                        + '<h6>' + new Date(sighting.dateTime).toDateString() + '</h6>'
                        + '<div class="edit" id="edit' + sighting.id + '">'
                        + '<a href="#"><i class="material-icons">edit</i></a></div>'
                        + '<div class="delete" id="delete' + sighting.id + '">'
                        + '<a href="#"><i class="material-icons">delete_forever</i></a></div>'
                        + '</div><hr/>');
                var locationString = '<p>' + sighting.location.name + ' ';
                if (sighting.location.description != null) {
                    locationString += '(' + sighting.location.description + ') ';
                }
                if (sighting.location.streetAddress != null) {
                    locationString += sighting.location.streetAddress + ', ';
                }
                if (sighting.location.city != null) {
                    locationString += sighting.location.city + ', ';
                }
                if (sighting.location.state != null) {
                    locationString += sighting.location.state + ', ';
                }
                if (sighting.location.country != null) {
                    locationString += sighting.location.country + ' ';
                }
                if (sighting.location.latitudeDeg != null && sighting.location.longitudeDeg != null) {
                    locationString += 'geo: ' + sighting.location.latitudeDeg + ',' + sighting.location.longitudeDeg;
                }
                locationString += '</p>';
                $('#search-results-div .' + sighting.id).append(locationString);
                ;
                for (i = 0; i < sighting.supers.length; i++) {
                    $('#search-results-div .' + sighting.id).append('<p>'
                            + '<i class="material-icons">person_pin_circle</i>&nbsp;'
                            + sighting.supers[i].name + '</p>');
                }
            });
        },
        error: function() {
            $('#load-error').show();
        }
    });
}

function fillEditForm(id) {
    $('div.form-group h5').text('Edit Sighting');
    $('#submit-button').hide();
    $('.edit-buttons').show();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/sighting/' + id,
        success: function(sighting, status) {
            $('#sighting-id').val(id);
            var dateTime = new Date(sighting.dateTime);
            var supers = [];
            $.each(sighting.supers, function(index, superPerson) {
                supers.push(superPerson.id);
            });
            $('#super-select').val(supers);
            $('#super-select').material_select();
            $('.datepicker').val(dateTime.toISOString().substr(0, 10));
            $('.timepicker').val(dateTime.toTimeString().substr(0, 8));
        },
        error: function() {
            alert('ERROR: Unable to populate the edit form.');
        }
    });
}

function editSighting() {
    var dateTime = Date.parse($('.datepicker').val() + ' ' + $('.timepicker').val());
    var locationId = Number($('#all-locations-list input[type="radio"][name="location-select"]:checked').val());
    var jsonSupers = [];
    $('#super-select option:selected').each(function() {
        jsonSupers.push({id: Number($(this).val())});
    });
    $.ajax({
        type: 'PATCH',
        url: 'http://localhost:8080/SuperheroSightings/sighting/' + $('#sighting-id').val(),
        data: JSON.stringify({
            id: Number($('#sighting-id').val()),
            dateTime: dateTime,
            locationId: locationId,
            supers: jsonSupers
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function() {
            clearEditForm();
            $('#edit-sighting-error').hide();
            $('#edit-sighting-success').show();
            $('#search-results-div').empty();
        },
        error: function() {
            $('#edit-sighting-success').hide();
            $('#edit-sighting-error').show();
        }
    });
}

function clearEditForm() {
    $('div.form-group form')[0].reset();
    $('#super-select').material_select();
    $('div.form-group h5').text('Report A Sighting');
    $('#submit-button').show();
    $('.edit-buttons').hide();
}

function deleteSighting(id) {
    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/SuperheroSightings/sighting/' + id,
        success: function() {
            alert('Sighting successfully deleted.');
            $('#search-results-div').empty();
        },
        error: function() {
            alert('ERROR: Unable to delete location.');
        }
    });
}