package com.sg.superherosightings.controller;

import com.sg.superherosightings.model.Sighting;
import com.sg.superherosightings.service.Service;
import java.time.LocalDate;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@CrossOrigin
@Controller
public class SightingRestController {

    @Inject
    private Service service;

    @RequestMapping(value = "/sighting", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void addSighting(@RequestBody Sighting sighting) {
        service.addSighting(sighting);
    }

    @RequestMapping(value = "/sighting/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Sighting getSighting(@PathVariable("id") Integer id) {
        return service.getSightingById(id);
    }

    @RequestMapping(value = "/sightings", method = RequestMethod.GET)
    @ResponseBody
    public List<Sighting> getAllSightings() {
        return service.getAllSightings();
    }

    @RequestMapping(value = "/sightings/recent", method = RequestMethod.GET)
    @ResponseBody
    public List<Sighting> getRecentSightings() {
        return service.getRecentSightings();
    }

    @RequestMapping(value = "/sightings/date/{date}", method = RequestMethod.GET)
    @ResponseBody
    public List<Sighting> getSightingsByDate(
            @PathVariable("date") Long dateInMilliseconds) {
        return service.getSightingsByDate(dateInMilliseconds);
    }

    @RequestMapping(value = "/sightings/super/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Sighting> getSightingsBySuper(
            @PathVariable("id") Integer superId) {
        return service.getSightingsBySuper(superId);
    }

    @RequestMapping(value = "/sighting/{id}", method = RequestMethod.PATCH)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSighting(@RequestBody Sighting sighting) {
        service.updateSighting(sighting);
    }

    @RequestMapping(value = "/sighting/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSighting(@PathVariable("id") Integer id) {
        service.deleteSighting(id);
    }
}
