package com.sg.superherosightings.controller;

import com.sg.superherosightings.model.Super;
import com.sg.superherosightings.service.Service;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@CrossOrigin
@Controller
public class SuperRestController {

    @Inject
    private Service service;

    @RequestMapping(value = "/super", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void addSuper(@RequestBody Super superPerson) {
        service.addSuper(superPerson);
    }

    @RequestMapping(value = "/super/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Super getSuper(@PathVariable("id") Integer id) {
        return service.getSuperById(id);
    }

    @RequestMapping(value = "/supers", method = RequestMethod.GET)
    @ResponseBody
    public List<Super> getAllSupers() {
        return service.getAllSupers();
    }

    @RequestMapping(value = "/supers/location/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Super> getSupersBySightingLocation(
            @PathVariable("id") Integer locationId) {
        return service.getSupersBySightingLocation(locationId);
    }

    @RequestMapping(value = "/super/{id}", method = RequestMethod.PATCH)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSuper(@RequestBody Super superPerson) {
        service.updateSuper(superPerson);
    }

    @RequestMapping(value = "/super/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSuper(@PathVariable("id") Integer id) {
        service.deleteSuper(id);
    }
}
