package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Location;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class LocationDaoImpl implements LocationDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_LOCATION
            = "INSERT INTO Location (name, description, country, state, city, "
            + "streetAddress, latitudeDeg, longitudeDeg) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

    private static final String SQL_SELECT_LOCATION
            = "SELECT * FROM Location WHERE id = ?;";

    private static final String SQL_SELECT_ALL_LOCATIONS
            = "SELECT * FROM Location;";

    private static final String SQL_SELECT_LOCATION_BY_SUPER_SIGHTING
            = "SELECT DISTINCT Location.* "
            + "FROM Super "
            + "INNER JOIN SuperSighting "
            + "ON Super.id = SuperSighting.superId "
            + "INNER JOIN Sighting "
            + "ON Sighting.id = SuperSighting.sightingId "
            + "INNER JOIN Location "
            + "ON Location.id = Sighting.locationId "
            + "WHERE Super.id = ?;";

    private static final String SQL_SELECT_LOCATION_BY_ORGANIZATION
            = "SELECT DISTINCT Location.* "
            + "FROM Organization "
            + "INNER JOIN OrganizationLocation "
            + "ON Organization.id = OrganizationLocation.organizationId "
            + "INNER JOIN Location "
            + "ON Location.id = OrganizationLocation.locationId "
            + "WHERE Organization.id = ?;";

    private static final String SQL_UPDATE_LOCATION
            = "UPDATE Location SET name = ?, description = ?, "
            + "country = ?, state = ?, city = ?, streetAddress = ?, "
            + "latitudeDeg = ?, longitudeDeg = ? "
            + "WHERE id = ?;";

    private static final String SQL_DELETE_ORGANIZATIONLOCATION
            = "DELETE FROM OrganizationLocation WHERE locationId = ?;";

    private static final String SQL_SELECT_SIGHTINGID_BY_LOCATION
            = "SELECT id FROM Sighting WHERE locationId = ?;";

    private static final String SQL_DELETE_SUPERSIGHTING
            = "DELETE FROM SuperSighting WHERE sightingId = ?;";

    private static final String SQL_DELETE_SIGHTING
            = "DELETE FROM Sighting WHERE id = ?;";

    private static final String SQL_DELETE_LOCATION
            = "DELETE FROM Location WHERE id = ?;";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addLocation(Location location) {
        jdbcTemplate.update(SQL_INSERT_LOCATION,
                location.getName(),
                location.getDescription(),
                location.getCountry(),
                location.getState(),
                location.getCity(),
                location.getStreetAddress(),
                location.getLatitudeDeg(),
                location.getLongitudeDeg());
        location.setId(jdbcTemplate.queryForObject(
                "SELECT LAST_INSERT_ID()", Integer.class));
    }

    @Override
    public Location getLocationById(Integer id) {
        try {
            return jdbcTemplate.queryForObject(
                    SQL_SELECT_LOCATION, new LocationMapper(), id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Location> getAllLocations() {
        return jdbcTemplate.query(
                SQL_SELECT_ALL_LOCATIONS, new LocationMapper());
    }

    @Override
    public List<Location> getLocationsWhereSuperHasBeenSighted(Integer superId) {
        return jdbcTemplate.query(SQL_SELECT_LOCATION_BY_SUPER_SIGHTING,
                new LocationMapper(), superId);
    }

    @Override
    public List<Location> getLocationsByOrganization(Integer organizationId) {
        return jdbcTemplate.query(SQL_SELECT_LOCATION_BY_ORGANIZATION,
                new LocationMapper(), organizationId);
    }

    @Override
    public void updateLocation(Location location) {
        jdbcTemplate.update(SQL_UPDATE_LOCATION,
                location.getName(),
                location.getDescription(),
                location.getCountry(),
                location.getState(),
                location.getCity(),
                location.getStreetAddress(),
                location.getLatitudeDeg(),
                location.getLongitudeDeg(),
                location.getId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteLocation(Integer id) {
        jdbcTemplate.update(SQL_DELETE_ORGANIZATIONLOCATION, id);
        List<Integer> sightingIds = jdbcTemplate.query(
                SQL_SELECT_SIGHTINGID_BY_LOCATION, new SightingIdMapper(), id);
        for (Integer sightingId : sightingIds) {
            jdbcTemplate.update(SQL_DELETE_SUPERSIGHTING, sightingId);
            jdbcTemplate.update(SQL_DELETE_SIGHTING, sightingId);
        }
        jdbcTemplate.update(SQL_DELETE_LOCATION, id);
    }

    private static final class LocationMapper implements RowMapper<Location> {

        @Override
        public Location mapRow(ResultSet rs, int i) throws SQLException {
            Location l = new Location();
            l.setId(rs.getInt("id"));
            l.setName(rs.getString("name"));
            l.setDescription(rs.getString("description"));
            l.setCountry(rs.getString("country"));
            l.setState(rs.getString("state"));
            l.setCity(rs.getString("city"));
            l.setStreetAddress(rs.getString("streetAddress"));
            l.setLatitudeDeg(rs.getBigDecimal("latitudeDeg"));
            l.setLongitudeDeg(rs.getBigDecimal("longitudeDeg"));
            return l;
        }
    }

    private static final class SightingIdMapper implements RowMapper<Integer> {

        @Override
        public Integer mapRow(ResultSet rs, int i) throws SQLException {
            return rs.getInt("id");
        }
    }
}
