package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Power;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class PowerDaoImpl implements PowerDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_POWER
            = "INSERT INTO Power (description) "
            + "VALUES (?);";

    private static final String SQL_SELECT_POWER
            = "SELECT * FROM Power WHERE id = ?;";

    private static final String SQL_SELECT_ALL_POWERS
            = "SELECT * FROM Power;";

    private static final String SQL_SELECT_POWER_BY_SUPER
            = "SELECT Power.* FROM Super "
            + "INNER JOIN SuperPower "
            + "ON Super.id = SuperPower.superId "
            + "INNER JOIN Power "
            + "ON Power.id = SuperPower.powerId "
            + "WHERE Super.id = ?;";

    private static final String SQL_UPDATE_POWER
            = "UPDATE Power SET description = ? WHERE id = ?;";

    private static final String SQL_DELETE_SUPERPOWER
            = "DELETE FROM SuperPower WHERE powerId = ?;";

    private static final String SQL_DELETE_POWER
            = "DELETE FROM Power WHERE id = ?;";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addPower(Power power) {
        jdbcTemplate.update(SQL_INSERT_POWER, power.getDescription());
        power.setId(jdbcTemplate.queryForObject(
                "SELECT LAST_INSERT_ID()", Integer.class));
    }

    @Override
    public Power getPowerById(Integer id) {
        try {
            return jdbcTemplate.queryForObject(
                    SQL_SELECT_POWER, new PowerMapper(), id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Power> getAllPowers() {
        return jdbcTemplate.query(SQL_SELECT_ALL_POWERS, new PowerMapper());
    }

    @Override
    public List<Power> getPowersBySuper(Integer superId) {
        return jdbcTemplate.query(SQL_SELECT_POWER_BY_SUPER,
                new PowerMapper(), superId);
    }

    @Override
    public void updatePower(Power power) {
        jdbcTemplate.update(SQL_UPDATE_POWER,
                power.getDescription(),
                power.getId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deletePower(Integer id) {
        jdbcTemplate.update(SQL_DELETE_SUPERPOWER, id);
        jdbcTemplate.update(SQL_DELETE_POWER, id);
    }

    private static final class PowerMapper implements RowMapper<Power> {

        @Override
        public Power mapRow(ResultSet rs, int i) throws SQLException {
            Power p = new Power();
            p.setId(rs.getInt("id"));
            p.setDescription(rs.getString("description"));
            return p;
        }
    }
}
