package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Sighting;
import com.sg.superherosightings.model.Super;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class SightingDaoImpl implements SightingDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_SIGHTING
            = "INSERT INTO Sighting (dateTime, locationId) "
            + "VALUES (?, ?);";

    private static final String SQL_INSERT_SUPERSIGHTING
            = "INSERT INTO SuperSighting (superId, sightingId) "
            + "VALUES (?, ?);";

    private static final String SQL_SELECT_SIGHTING
            = "SELECT * FROM Sighting WHERE id = ?;";

    private static final String SQL_SELECT_ALL_SIGHTINGS
            = "SELECT * FROM Sighting;";

    private static final String SQL_SELECT_SIGHTING_BY_DATE
            = "SELECT * FROM Sighting "
            + "WHERE DATE(dateTime) = ?;";

    private static final String SQL_SELECT_SIGHTING_BY_SUPER
            = "SELECT Sighting.*, Location.* FROM Super "
            + "INNER JOIN SuperSighting "
            + "ON Super.id = SuperSighting.superId "
            + "INNER JOIN Sighting "
            + "ON Sighting.id = SuperSighting.sightingId "
            + "INNER JOIN Location "
            + "ON Location.id = Sighting.locationId "
            + "WHERE Super.id = ?;";

    private static final String SQL_UPDATE_SIGHTING
            = "UPDATE Sighting SET dateTime = ?, locationId = ? "
            + "WHERE id = ?;";

    private static final String SQL_DELETE_SUPERSIGHTING
            = "DELETE FROM SuperSighting WHERE sightingId = ?;";

    private static final String SQL_DELETE_SIGHTING
            = "DELETE FROM Sighting WHERE id = ?;";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addSighting(Sighting sighting) {
        jdbcTemplate.update(SQL_INSERT_SIGHTING,
                sighting.getDateTime().toString(),
                sighting.getLocationId());
        sighting.setId(jdbcTemplate.queryForObject(
                "SELECT LAST_INSERT_ID()", Integer.class));
        if (sighting.getSupers() != null) {
            insertSuperSighting(sighting);
        }
    }

    @Override
    public Sighting getSightingById(Integer id) {
        try {
            return jdbcTemplate.queryForObject(
                    SQL_SELECT_SIGHTING, new SightingMapper(), id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Sighting> getAllSightings() {
        return jdbcTemplate.query(
                SQL_SELECT_ALL_SIGHTINGS, new SightingMapper());
    }

    @Override
    public List<Sighting> getSightingsByDate(LocalDate date) {
        return jdbcTemplate.query(SQL_SELECT_SIGHTING_BY_DATE,
                new SightingMapper(), date.toString());
    }

    @Override
    public List<Sighting> getSightingsBySuper(Integer superId) {
        return jdbcTemplate.query(SQL_SELECT_SIGHTING_BY_SUPER,
                new SightingMapper(), superId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateSighting(Sighting sighting) {
        jdbcTemplate.update(SQL_UPDATE_SIGHTING,
                sighting.getDateTime().toString(),
                sighting.getLocationId(),
                sighting.getId());
        jdbcTemplate.update(SQL_DELETE_SUPERSIGHTING, sighting.getId());
        if (sighting.getSupers() != null) {
            insertSuperSighting(sighting);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteSighting(Integer id) {
        jdbcTemplate.update(SQL_DELETE_SUPERSIGHTING, id);
        jdbcTemplate.update(SQL_DELETE_SIGHTING, id);
    }

    private void insertSuperSighting(Sighting sighting) {
        List<Super> supers = sighting.getSupers();
        for (Super superPerson : supers) {
            jdbcTemplate.update(SQL_INSERT_SUPERSIGHTING,
                    superPerson.getId(), sighting.getId());
        }
    }

    private static final class SightingMapper implements RowMapper<Sighting> {

        @Override
        public Sighting mapRow(ResultSet rs, int i) throws SQLException {
            Sighting s = new Sighting();
            s.setId(rs.getInt("id"));
            s.setDateTime(Timestamp.valueOf(rs.getString("dateTime")));
            s.setLocationId(rs.getInt("locationId"));
            return s;
        }
    }
}
