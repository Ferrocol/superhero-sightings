package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Power;
import java.util.List;

public interface PowerDao {

    void addPower(Power power);

    Power getPowerById(Integer id);

    List<Power> getAllPowers();

    List<Power> getPowersBySuper(Integer superId);

    void updatePower(Power power);

    void deletePower(Integer id);
}
