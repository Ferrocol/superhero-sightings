package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Organization;
import com.sg.superherosightings.model.Sighting;
import com.sg.superherosightings.model.Super;

public interface Builder {

    Super buildCompleteSuper(Super superPerson);

    Organization buildCompleteOrganization(Organization organization);

    Sighting buildCompleteSighting(Sighting sighting);
}
