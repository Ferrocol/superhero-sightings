package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Location;
import com.sg.superherosightings.model.Organization;
import com.sg.superherosightings.model.Super;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class OrganizationDaoImpl implements OrganizationDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_ORGANIZATION
            = "INSERT INTO Organization (name, description, phone, email) "
            + "VALUES (?, ?, ?, ?);";

    private static final String SQL_INSERT_SUPERORGANIZATION
            = "INSERT INTO SuperOrganization (superId, organizationId) "
            + "VALUES (?, ?);";

    private static final String SQL_INSERT_ORGANIZATIONLOCATION
            = "INSERT INTO OrganizationLocation (organizationId, locationId) "
            + "VALUES (?, ?);";

    private static final String SQL_SELECT_ORGANIZATION
            = "SELECT * FROM Organization WHERE id = ?;";

    private static final String SQL_SELECT_ALL_ORGANIZATIONS
            = "SELECT * FROM Organization;";

    private static final String SQL_SELECT_ORGANIZATION_BY_SUPER
            = "SELECT Organization.* "
            + "FROM Super "
            + "INNER JOIN SuperOrganization "
            + "ON Super.id = SuperOrganization.superId "
            + "INNER JOIN Organization "
            + "ON Organization.id = SuperOrganization.organizationId "
            + "WHERE Super.id = ?;";

    private static final String SQL_UPDATE_ORGANIZATION
            = "UPDATE Organization SET name = ?, description = ?, "
            + "phone = ?, email = ? "
            + "WHERE id = ?;";

    private static final String SQL_DELETE_SUPERORGANIZATION
            = "DELETE FROM SuperOrganization WHERE organizationId = ?;";

    private static final String SQL_DELETE_ORGANIZATIONLOCATION
            = "DELETE FROM OrganizationLocation WHERE organizationId = ?;";

    private static final String SQL_DELETE_ORGANIZATION
            = "DELETE FROM Organization WHERE id = ?;";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addOrganization(Organization organization) {
        jdbcTemplate.update(SQL_INSERT_ORGANIZATION,
                organization.getName(),
                organization.getDescription(),
                organization.getPhone(),
                organization.getEmail());
        organization.setId(jdbcTemplate.queryForObject(
                "SELECT LAST_INSERT_ID()", Integer.class));
        if (organization.getSupers() != null) {
            insertSuperOrganization(organization);
        }
        if (organization.getLocations() != null) {
            insertOrganizationLocation(organization);
        }
    }

    @Override
    public Organization getOrganizationById(Integer id) {
        try {
            return jdbcTemplate.queryForObject(
                    SQL_SELECT_ORGANIZATION, new OrganizationMapper(), id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Organization> getAllOrganizations() {
        return jdbcTemplate.query(
                SQL_SELECT_ALL_ORGANIZATIONS, new OrganizationMapper());
    }

    @Override
    public List<Organization> getOrganizationsBySuper(Integer superId) {
        return jdbcTemplate.query(SQL_SELECT_ORGANIZATION_BY_SUPER,
                new OrganizationMapper(), superId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateOrganization(Organization organization) {
        Integer id = organization.getId();
        jdbcTemplate.update(SQL_UPDATE_ORGANIZATION,
                organization.getName(),
                organization.getDescription(),
                organization.getPhone(),
                organization.getEmail(),
                organization.getId());
        jdbcTemplate.update(SQL_DELETE_SUPERORGANIZATION, id);
        jdbcTemplate.update(SQL_DELETE_ORGANIZATIONLOCATION, id);
        if (organization.getSupers() != null) {
            insertSuperOrganization(organization);
        }
        if (organization.getLocations() != null) {
            insertOrganizationLocation(organization);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteOrganization(Integer id) {
        jdbcTemplate.update(SQL_DELETE_SUPERORGANIZATION, id);
        jdbcTemplate.update(SQL_DELETE_ORGANIZATIONLOCATION, id);
        jdbcTemplate.update(SQL_DELETE_ORGANIZATION, id);
    }

    private void insertSuperOrganization(Organization organization) {
        List<Super> supers = organization.getSupers();
        for (Super superPerson : supers) {
            jdbcTemplate.update(SQL_INSERT_SUPERORGANIZATION,
                    superPerson.getId(), organization.getId());
        }
    }

    private void insertOrganizationLocation(Organization organization) {
        List<Location> locations = organization.getLocations();
        for (Location location : locations) {
            jdbcTemplate.update(SQL_INSERT_ORGANIZATIONLOCATION,
                    organization.getId(), location.getId());
        }
    }

    private static final class OrganizationMapper
            implements RowMapper<Organization> {

        @Override
        public Organization mapRow(ResultSet rs, int i) throws SQLException {
            Organization o = new Organization();
            o.setId(rs.getInt("id"));
            o.setName(rs.getString("name"));
            o.setDescription(rs.getString("description"));
            o.setPhone(rs.getString("phone"));
            o.setEmail(rs.getString("email"));
            return o;
        }
    }
}
