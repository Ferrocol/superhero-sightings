package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Organization;
import com.sg.superherosightings.model.Sighting;
import com.sg.superherosightings.model.Super;

public class BuilderImpl implements Builder {

    private final SuperDao superDao;
    private final PowerDao powerDao;
    private final LocationDao locationDao;
    private final OrganizationDao organizationDao;
    private final SightingDao sightingDao;

    public BuilderImpl(SuperDao superDao, PowerDao powerDao,
            LocationDao locationDao, OrganizationDao organizationDao,
            SightingDao sightingDao) {
        this.superDao = superDao;
        this.powerDao = powerDao;
        this.locationDao = locationDao;
        this.organizationDao = organizationDao;
        this.sightingDao = sightingDao;
    }

    @Override
    public Super buildCompleteSuper(Super superPerson) {
        Integer superId = superPerson.getId();
        superPerson.setPowers(powerDao.getPowersBySuper(superId));
        superPerson.setOrganizations(organizationDao.getOrganizationsBySuper(superId));
        superPerson.setSightings(sightingDao.getSightingsBySuper(superId));
        for (Sighting sighting : superPerson.getSightings()) {
            sighting.setLocation(locationDao.getLocationById(sighting.getLocationId()));
        }
        return superPerson;
    }

    @Override
    public Organization buildCompleteOrganization(Organization organization) {
        Integer organizationId = organization.getId();
        organization.setLocations(locationDao.getLocationsByOrganization(organizationId));
        organization.setSupers(superDao.getSupersByOrganization(organizationId));
        for (Super superPerson : organization.getSupers()) {
            superPerson.setPowers(powerDao.getPowersBySuper(superPerson.getId()));
        }
        return organization;
    }

    @Override
    public Sighting buildCompleteSighting(Sighting sighting) {
        sighting.setLocation(locationDao.getLocationById(sighting.getLocationId()));
        sighting.setSupers(superDao.getSupersBySighting(sighting.getId()));
        for (Super superPerson : sighting.getSupers()) {
            Integer superId = superPerson.getId();
            superPerson.setPowers(powerDao.getPowersBySuper(superId));
            superPerson.setOrganizations(organizationDao.getOrganizationsBySuper(superId));
        }
        return sighting;
    }
}
