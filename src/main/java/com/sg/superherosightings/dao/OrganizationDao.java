package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Organization;
import java.util.List;

public interface OrganizationDao {

    void addOrganization(Organization organization);

    Organization getOrganizationById(Integer id);

    List<Organization> getAllOrganizations();

    List<Organization> getOrganizationsBySuper(Integer superId);

    void updateOrganization(Organization organization);

    void deleteOrganization(Integer id);
}
