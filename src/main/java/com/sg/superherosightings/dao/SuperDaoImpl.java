package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Organization;
import com.sg.superherosightings.model.Power;
import com.sg.superherosightings.model.Sighting;
import com.sg.superherosightings.model.Super;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class SuperDaoImpl implements SuperDao {

    private static final String SQL_INSERT_SUPER
            = "INSERT INTO Super (name, description) "
            + "VALUES (?, ?);";

    private static final String SQL_INSERT_SUPERPOWER
            = "INSERT INTO SuperPower (superId, powerId) "
            + "VALUES (?, ?);";

    private static final String SQL_INSERT_SUPERORGANIZATION
            = "INSERT INTO SuperOrganization (superId, organizationId) "
            + "VALUES (?, ?);";

    private static final String SQL_INSERT_SUPERSIGHTING
            = "INSERT INTO SuperSighting (superId, sightingId) "
            + "VALUES (?, ?);";

    private static final String SQL_SELECT_SUPER
            = "SELECT * FROM Super WHERE id = ?;";

    private static final String SQL_SELECT_ALL_SUPERS
            = "SELECT * FROM Super;";

    private static final String SQL_SELECT_SUPER_BY_SIGHTING_LOCATION
            = "SELECT DISTINCT Super.* "
            + "FROM Location "
            + "INNER JOIN Sighting "
            + "ON Location.id = Sighting.locationId "
            + "INNER JOIN SuperSighting "
            + "ON Sighting.id = SuperSighting.sightingId "
            + "INNER JOIN Super "
            + "ON Super.id = SuperSighting.superId "
            + "WHERE Location.id = ?;";

    private static final String SQL_SELECT_SUPER_BY_SIGHTING
            = "SELECT Super.* "
            + "FROM Sighting "
            + "INNER JOIN SuperSighting "
            + "ON Sighting.id = SuperSighting.sightingId "
            + "INNER JOIN Super "
            + "ON Super.id = SuperSighting.superId "
            + "WHERE Sighting.id = ?;";

    private static final String SQL_SELECT_SUPER_BY_ORGANIZATION
            = "SELECT Super.* "
            + "FROM Organization "
            + "INNER JOIN SuperOrganization "
            + "ON Organization.id = SuperOrganization.organizationId "
            + "INNER JOIN Super "
            + "ON Super.id = SuperOrganization.superId "
            + "WHERE Organization.id = ?;";

    private static final String SQL_UPDATE_SUPER
            = "UPDATE Super SET name = ?, description = ? "
            + "WHERE id = ?;";

    private static final String SQL_DELETE_SUPERPOWER
            = "DELETE FROM SuperPower WHERE superId = ?;";

    private static final String SQL_DELETE_SUPERORGANIZATION
            = "DELETE FROM SuperOrganization WHERE superId = ?;";

    private static final String SQL_DELETE_SUPERSIGHTING
            = "DELETE FROM SuperSighting WHERE superId = ?;";

    private static final String SQL_DELETE_SUPER
            = "DELETE FROM Super WHERE id = ?;";

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addSuper(Super superPerson) {
        jdbcTemplate.update(SQL_INSERT_SUPER,
                superPerson.getName(),
                superPerson.getDescription());
        superPerson.setId(jdbcTemplate.queryForObject(
                "SELECT LAST_INSERT_ID()", Integer.class));
        if (superPerson.getPowers() != null) {
            insertSuperPower(superPerson);
        }
        if (superPerson.getOrganizations() != null) {
            insertSuperOrganization(superPerson);
        }
        if (superPerson.getSightings() != null) {
            insertSuperSighting(superPerson);
        }
    }

    @Override
    public Super getSuperById(Integer id) {
        try {
            return jdbcTemplate.queryForObject(
                    SQL_SELECT_SUPER, new SuperMapper(), id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Super> getAllSupers() {
        return jdbcTemplate.query(SQL_SELECT_ALL_SUPERS, new SuperMapper());
    }

    @Override
    public List<Super> getSupersBySightingLocation(Integer locationId) {
        return jdbcTemplate.query(SQL_SELECT_SUPER_BY_SIGHTING_LOCATION,
                new SuperMapper(), locationId);
    }

    @Override
    public List<Super> getSupersBySighting(Integer sightingId) {
        return jdbcTemplate.query(SQL_SELECT_SUPER_BY_SIGHTING,
                new SuperMapper(), sightingId);
    }

    @Override
    public List<Super> getSupersByOrganization(Integer organizationId) {
        return jdbcTemplate.query(SQL_SELECT_SUPER_BY_ORGANIZATION,
                new SuperMapper(), organizationId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateSuper(Super superPerson) {
        Integer id = superPerson.getId();
        jdbcTemplate.update(SQL_UPDATE_SUPER,
                superPerson.getName(),
                superPerson.getDescription(),
                id);
        jdbcTemplate.update(SQL_DELETE_SUPERPOWER, id);
        jdbcTemplate.update(SQL_DELETE_SUPERORGANIZATION, id);
        jdbcTemplate.update(SQL_DELETE_SUPERSIGHTING, id);
        if (superPerson.getPowers() != null) {
            insertSuperPower(superPerson);
        }
        if (superPerson.getOrganizations() != null) {
            insertSuperOrganization(superPerson);
        }
        if (superPerson.getSightings() != null) {
            insertSuperSighting(superPerson);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteSuper(Integer id) {
        jdbcTemplate.update(SQL_DELETE_SUPERPOWER, id);
        jdbcTemplate.update(SQL_DELETE_SUPERORGANIZATION, id);
        jdbcTemplate.update(SQL_DELETE_SUPERSIGHTING, id);
        jdbcTemplate.update(SQL_DELETE_SUPER, id);
    }

    private void insertSuperPower(Super superPerson) {
        List<Power> powers = superPerson.getPowers();
        for (Power power : powers) {
            jdbcTemplate.update(SQL_INSERT_SUPERPOWER,
                    superPerson.getId(), power.getId());
        }
    }

    private void insertSuperOrganization(Super superPerson) {
        List<Organization> organizations = superPerson.getOrganizations();
        for (Organization organization : organizations) {
            jdbcTemplate.update(SQL_INSERT_SUPERORGANIZATION,
                    superPerson.getId(), organization.getId());
        }
    }

    private void insertSuperSighting(Super superPerson) {
        List<Sighting> sightings = superPerson.getSightings();
        for (Sighting sighting : sightings) {
            jdbcTemplate.update(SQL_INSERT_SUPERSIGHTING, superPerson.getId(), sighting.getId());
        }
    }

    private static final class SuperMapper implements RowMapper<Super> {

        @Override
        public Super mapRow(ResultSet rs, int i) throws SQLException {
            Super s = new Super();
            s.setId(rs.getInt("id"));
            s.setName(rs.getString("name"));
            s.setDescription(rs.getString("description"));
            return s;
        }
    }
}
