package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Sighting;
import java.time.LocalDate;
import java.util.List;

public interface SightingDao {

    void addSighting(Sighting sighting);

    Sighting getSightingById(Integer id);

    List<Sighting> getAllSightings();

    List<Sighting> getSightingsByDate(LocalDate date);

    List<Sighting> getSightingsBySuper(Integer superId);

    void updateSighting(Sighting sighting);

    void deleteSighting(Integer id);
}
