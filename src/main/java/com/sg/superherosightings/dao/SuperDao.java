package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Super;
import java.util.List;

public interface SuperDao {

    void addSuper(Super superPerson);

    Super getSuperById(Integer id);

    List<Super> getAllSupers();

    List<Super> getSupersBySightingLocation(Integer locationId);

    List<Super> getSupersBySighting(Integer sightingId);

    List<Super> getSupersByOrganization(Integer organizationId);

    void updateSuper(Super superPerson);

    void deleteSuper(Integer id);
}
