package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Location;
import java.util.List;

public interface LocationDao {

    void addLocation(Location location);

    Location getLocationById(Integer id);

    List<Location> getAllLocations();

    List<Location> getLocationsWhereSuperHasBeenSighted(Integer superId);

    List<Location> getLocationsByOrganization(Integer organizationId);

    void updateLocation(Location location);

    void deleteLocation(Integer id);
}
