package com.sg.superherosightings.service;

import com.sg.superherosightings.model.Location;
import com.sg.superherosightings.model.Organization;
import com.sg.superherosightings.model.Power;
import com.sg.superherosightings.model.Sighting;
import com.sg.superherosightings.model.Super;
import java.util.List;

public interface Service {

    void addSuper(Super superPerson);

    Super getSuperById(Integer superId);

    List<Super> getAllSupers();

    List<Super> getSupersBySightingLocation(Integer locationId);

    void updateSuper(Super superPerson);

    void deleteSuper(Integer superId);

    void addPower(Power power);

    Power getPowerById(Integer powerId);

    List<Power> getAllPowers();

    void updatePower(Power power);

    void deletePower(Integer powerId);

    void addLocation(Location location);

    Location getLocationById(Integer locationId);

    List<Location> getAllLocations();

    List<Location> getLocationsWhereSuperHasBeenSighted(Integer superId);

    void updateLocation(Location location);

    void deleteLocation(Integer locationId);

    void addOrganization(Organization organization);

    Organization getOrganizationById(Integer organizationId);

    List<Organization> getAllOrganizations();

    void updateOrganization(Organization organization);

    void deleteOrganization(Integer organizationId);

    void addSighting(Sighting sighting);

    Sighting getSightingById(Integer sightingId);

    List<Sighting> getAllSightings();

    List<Sighting> getRecentSightings();

    List<Sighting> getSightingsByDate(Long dateInMilliseconds);

    List<Sighting> getSightingsBySuper(Integer superId);

    void updateSighting(Sighting sighting);

    void deleteSighting(Integer sightingId);
}
