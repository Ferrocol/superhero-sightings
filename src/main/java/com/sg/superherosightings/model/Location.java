package com.sg.superherosightings.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Location {

    private Integer id;
    private String name;
    private String description;
    private String country;
    private String state;
    private String city;
    private String streetAddress;
    private BigDecimal latitudeDeg;
    private BigDecimal longitudeDeg;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public BigDecimal getLatitudeDeg() {
        return latitudeDeg;
    }

    public void setLatitudeDeg(BigDecimal latitudeDeg) {
        this.latitudeDeg = latitudeDeg;
    }

    public BigDecimal getLongitudeDeg() {
        return longitudeDeg;
    }

    public void setLongitudeDeg(BigDecimal longitudeDeg) {
        this.longitudeDeg = longitudeDeg;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.id);
        hash = 17 * hash + Objects.hashCode(this.name);
        hash = 17 * hash + Objects.hashCode(this.description);
        hash = 17 * hash + Objects.hashCode(this.country);
        hash = 17 * hash + Objects.hashCode(this.state);
        hash = 17 * hash + Objects.hashCode(this.city);
        hash = 17 * hash + Objects.hashCode(this.streetAddress);
        hash = 17 * hash + Objects.hashCode(this.latitudeDeg);
        hash = 17 * hash + Objects.hashCode(this.longitudeDeg);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Location other = (Location) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.state, other.state)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.streetAddress, other.streetAddress)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.latitudeDeg, other.latitudeDeg)) {
            return false;
        }
        if (!Objects.equals(this.longitudeDeg, other.longitudeDeg)) {
            return false;
        }
        return true;
    }
}
