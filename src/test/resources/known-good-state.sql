USE HERO_DB_test;

SET SQL_SAFE_UPDATES = 0;

DELETE FROM SuperSighting;
DELETE FROM Sighting;
DELETE FROM OrganizationLocation;
DELETE FROM Location;
DELETE FROM SuperOrganization;
DELETE FROM Organization;
DELETE FROM SuperPower;
DELETE FROM Power;
DELETE FROM `Super`;

INSERT INTO `Super` (id, `name`, description) VALUES (1, 'Default Super', 'Default Alter Ego');

INSERT INTO Power (id, description) VALUES (1, 'Default Power');

INSERT INTO SuperPower (superId, powerId) VALUES (1, 1);

INSERT INTO Organization (id, `name`, description, phone, email) VALUES (1, 'Default Organization', 'Default Description', '123-456-7890', 'default@email.com');

INSERT INTO SuperOrganization (superId, organizationId) VALUES (1, 1);

INSERT INTO Location (id, `name`, description, country, `state`, city, streetAddress, latitudeDeg, longitudeDeg) VALUES (1, 'Default Location', 'Default Description', 'NA', 'Default State', 'Default City', 'Default Address', 12.3456789, 12.34567890);

INSERT INTO OrganizationLocation (organizationId, locationId) VALUES (1, 1);

INSERT INTO Sighting (id, `dateTime`, locationId) VALUES (1, '1000-01-01 00:00:00', 1);

INSERT INTO SuperSighting (superId, sightingId) VALUES (1, 1);

SET SQL_SAFE_UPDATES = 1;