package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Sighting;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SightingDaoImplTest {

    private SightingDao dao;

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                "test-applicationContext.xml");
        dao = ctx.getBean("sightingDao", SightingDao.class);
//        DatabaseInitializer.setKnownGoodState();
    }

    @Test
    public void testAddGetDeleteSighting() {
        Sighting sighting = new Sighting();
        Timestamp dateTime = Timestamp.valueOf(
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        sighting.setDateTime(dateTime);
        sighting.setLocationId(1);
        dao.addSighting(sighting);
        Integer sightingId = sighting.getId();
        Sighting fromDb = dao.getSightingById(sightingId);
        assertEquals(sighting, fromDb);
        dao.deleteSighting(sightingId);
        assertNull(dao.getSightingById(sightingId));
    }

    @Test
    public void testGetAllSightings() {
        List<Sighting> sightings = dao.getAllSightings();
        assertEquals(1, sightings.size());
        assertEquals((Integer) 1, sightings.get(0).getId());
    }

    @Test
    public void testGetSightingsByDate() {
        LocalDate date = LocalDate.of(1000, Month.JANUARY, 1);
        List<Sighting> sightingsByDefaultDate = dao.getSightingsByDate(date);
        assertEquals(1, sightingsByDefaultDate.size());
        assertEquals((Integer) 1, sightingsByDefaultDate.get(0).getId());
    }

    @Test
    public void testGetSightingsBySuper() {
        List<Sighting> sightingOfDefaultSuper = dao.getSightingsBySuper(1);
        assertEquals(1, sightingOfDefaultSuper.size());
        assertEquals((Integer) 1, sightingOfDefaultSuper.get(0).getId());
    }

    @Test
    public void testAddUpdateDeleteSighting() {
        Sighting sighting = new Sighting();
        Timestamp dateTime = Timestamp.valueOf(LocalDateTime.of(
                2000, Month.JANUARY, 1, 1, 1, 1));
        sighting.setDateTime(dateTime);
        sighting.setLocationId(1);
        dao.addSighting(sighting);
        sighting.setDateTime(Timestamp.valueOf(
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)));
        dao.updateSighting(sighting);
        Integer sightingId = sighting.getId();
        Sighting fromDb = dao.getSightingById(sightingId);
        assertEquals(sighting, fromDb);
        dao.deleteSighting(sightingId);
        assertNull(dao.getSightingById(sightingId));
    }
}
