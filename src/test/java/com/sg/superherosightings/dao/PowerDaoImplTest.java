package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Power;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PowerDaoImplTest {

    private PowerDao dao;

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                "test-applicationContext.xml");
        dao = ctx.getBean("powerDao", PowerDao.class);
//        DatabaseInitializer.setKnownGoodState();
    }

    @Test
    public void testAddGetDeletePower() {
        Power power = new Power();
        power.setDescription("New Power");
        dao.addPower(power);
        Integer powerId = power.getId();
        Power fromDb = dao.getPowerById(powerId);
        assertEquals(power, fromDb);
        dao.deletePower(powerId);
        assertNull(dao.getPowerById(powerId));
    }

    @Test
    public void testGetAllPowers() {
        List<Power> powers = dao.getAllPowers();
        assertNotEquals(0, powers.size());
    }

    @Test
    public void testGetPowersBySuper() {
        List<Power> powers = dao.getPowersBySuper(1);
        assertEquals(1, powers.size());
        assertEquals("Default Power", powers.get(0).getDescription());
    }

    @Test
    public void testAddUpdateDeletePower() {
        Power power = new Power();
        power.setDescription("Old Power");
        dao.addPower(power);
        power.setDescription("New Power");
        dao.updatePower(power);
        Integer powerId = power.getId();
        Power fromDb = dao.getPowerById(powerId);
        assertEquals(power, fromDb);
        dao.deletePower(powerId);
        assertNull(dao.getPowerById(powerId));
    }
}
